## RAE411 - Final project: Exam problem 

You have here the final project of the Telecommunication Software subject. 
You can find all of my codes. There are the part where I tried to do the react.js with the Facebook login. And the other I tried to do the client/server with python. 


As following: 
#### Inspiration
The links that the teacher gave in sakai. 

#### What the project does
First, there is a login to facebook. And then, there are a client/server tcp and a chat room.

#### Built with
React.Js, Facebook developpers, Heroku were used for the login.
Anaconda and spyder were the software for the client/server and the chat room. 

#### How I built it
Thanks to JavaScript, CSS/HTML and python.

#### Instructions for a user
For the facebook login, the user needs to have an facebook account and enter the email and then the password. 
For the Client/Server, there are two possibilities. The first one is to communicate two computers together, so one run one part of the code and the other the second part. Or the other possibility is to run on the same computer but in two consoles. 

#### Challenges I ran into
The challenging part was that I only knew python, and a little bit of database that were useful.

#### Accomplishments that I'm proud of 
I'm proud of me for trying to do each homework that were assigned even though I didn't always suceed. 

#### What I learned
I learned a lot, many tools that there are going to be very useful. 

#### What's next for my project
Try and suceed to link Client/Server/Data, analysed, the data put more login and sugest a good idea of a project. 
