# -*- coding: utf-8 -*-


""" 
     Mini Client/Server in TCP
"""

""" SERVER """

import socket

TCP_IP = '127.0.0.1'
TCP_PORT = 5009                 
BUFFER_SIZE = 1024               

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.bind((TCP_IP, TCP_PORT))  # Ask to the OS  to associate the socket to a ip/port

s.listen(1)   # transform the socket to connexion socket 
print("« Waiting client… »")
scom, caddr = s.accept()
print("« client connected with address : »", caddr)
data = scom.recv(BUFFER_SIZE)
print("received data:", data.decode('utf-8'))
scom.send(data)  # echo

scom.close()
s.close()
