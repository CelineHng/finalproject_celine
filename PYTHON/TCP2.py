""" 
    Mini Client in TCP
    
    CLIENT
"""

import socket

TCP_IP = '127.0.0.1'  # IP of the server to which we want to be connected
TCP_PORT = 5009	 # port of the server
BUFFER_SIZE = 1024          		# size of reading

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("« connecting to : »", TCP_IP, TCP_PORT)
s.connect((TCP_IP, TCP_PORT))
#Why a connect could not work ?
#1. the IP adress does not exist 
#2. No sock waiting for a connexion by accept on the port 
#3. No ways to the network adress in the port indicated

print("sending data...")
raw_data = "HELLO".encode('utf-8')
s.send(raw_data)

data = s.recv(BUFFER_SIZE)
print("received data:\n", data.decode('utf-8'))

s.close()
