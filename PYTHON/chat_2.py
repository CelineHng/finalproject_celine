
import socket

IP_R   = "127.0.0.1"    # addressee
PORT_R = 60001        # addressee

IP_D   = "127.0.0.1"    # receipt
PORT_D = 60002        # receipt

sock_e = socket.socket(socket.AF_INET,    # Osi Lvl3 Internet
                     socket.SOCK_DGRAM)   # Osi Lvl 4 - UDP

sock_r = socket.socket(socket.AF_INET,    # Osi Lvl3 Internet
                     socket.SOCK_DGRAM)   # Osi Lvl 4 - UDP

print("binding to:", IP_R,PORT_R)
sock_r.bind( (IP_R,PORT_R) )                      

msg_r = ""
msg_e = ""

while msg_r != "end" or msg_e != "end":
    # envoie
    msg_e = input("me  : ")
    raw_data = msg_e.encode('utf-8')
    sock_e.sendto(raw_data,(IP_D,PORT_D)) # - fin -
    # reception
    raw_data,addr=sock_r.recvfrom(1024)
    msg_r = raw_data.decode('utf-8')
    print("someone: ", msg_r)


sock_e.close()
sock_r.close()
print("end")

